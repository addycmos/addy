
# Table of Contents

1.  [AddyCMOS Addy Documentation: Capman](#orga64ad8d)
    1.  [About](#orgd84a109)
    2.  [Using Capman](#org91522b5)
    3.  [Making A Capsule](#org5e9e2cc)
    4.  [Installing A Capsule](#orgb6df162)


<a id="orga64ad8d"></a>

# AddyCMOS Addy Documentation: Capman


<a id="orgd84a109"></a>

## About

Capman is the capsule manager for installing mining software. It's similar to a package manager for Addy.


<a id="org91522b5"></a>

## Using Capman

Capman has a help option which you can use to get a list of options for Capman.

You can get a list of available options with:

    capman --help

and

    capman -h

The list will look like so.

      Usage: capman [ARGUMENT] [CAPSULE]
    
      --install       install capsule
      --make-capsule  make into capsule
      --help, -h      display help
    
    [CAPSULE] is only mandatory for some arguments
    
    Report any bugs to WheezyBackports#7260 on discord.


<a id="org5e9e2cc"></a>

## Making A Capsule

By default there are no .capsule files with Addy. There are however premade capsules that need to be created.

For this example we'll be making the xmrig-v6.18.0 capsule. Capsules are stored in the capsules/ directory, so make sure to cd into that.

    cd capsules/

To make our capsule we'll need to use capman.

    capman --make-capsule xmrig-v6.18.0

We'll get an output like so.

    ===> Directory of capsule to make found
    ===> Creating capsule archive
    xmrig-v6.18.0/
    xmrig-v6.18.0/xmrig_service
    xmrig-v6.18.0/xmrig_run
    xmrig-v6.18.0/inst_capsule.sh

Our capsule should show up in a directory listing like this.

    xmrig-v6.18.0.capsule

Now that our capsule is built we can move on to installing it.


<a id="orgb6df162"></a>

## Installing A Capsule

After we have built our capsule we can now install it like so.

    capman --install xmrig-v6.18.0.capsule

You should get an output like so.

    ===> Extracting capsule
    xmrig-v6.18.0/
    xmrig-v6.18.0/xmrig_service
    xmrig-v6.18.0/xmrig_run
    xmrig-v6.18.0/inst_capsule.sh
    ===> Downloading capsule
    (I left out the wget log because it's just gonna fill the screen with information we don't need)
    ===> Installing capsule

To check that our new capsule has been installed correctly we can use Miner Manager to check for xmrig-v6.18.0.

    mm --manage-miner --list-installed

It should show up like so.

    ===> teamredminer-v0.10.2
    ===> /home/addycmos/addy/miners/teamredminer-v0.10.2/init/teamredminer-v0.10.2_service exists
    ===> /home/addycmos/addy/miners/teamredminer-v0.10.2/teamredminer-v0.10.2_run exists
    ===> xmrig-v6.18.0
    ===> /home/addycmos/addy/miners/xmrig-v6.18.0/init/xmrig-v6.18.0_service exists
    ===> /home/addycmos/addy/miners/xmrig-v6.18.0/xmrig-v6.18.0_run exists

