Table of Contents
_________________

1. AddyCMOS Addy Documentation: Miner Manager
.. 1. About
.. 2. Using Miner Manager
.. 3. List Installed Miners
.. 4. Running Miners
..... 1. Configuration
..... 2. Actually Running The Miner
.. 5. List Currently Running Miners
.. 6. Attaching A Miner Session
.. 7. Enabling The Miner On System Boot


1 AddyCMOS Addy Documentation: Miner Manager
============================================

1.1 About
~~~~~~~~~

  Miner Manager (mm) is the miner management script for Addy. It
  controls start/stop operations, attachment of miner sessions, and
  enabling/disabling them on boot.


1.2 Using Miner Manager
~~~~~~~~~~~~~~~~~~~~~~~

  Miner Manager has a help option which you can use to get a list of
  options for Miner Manager.

  You can get a list of available options with:
  ,----
  | mm --help
  `----

  and

  ,----
  | mm -h
  `----

  The list will look like so.
  ,----
  | Usage: mm [ARGUMENT] [SUB-ARGUMENT] [MINER]
  | 
  |   --manage-miner           miner management
  |    --start [MINER]         start miner
  |    --stop [MINER]          stop miner
  |    --enable-boot [MINER]   enable miner on boot
  |    --disable-boot [MINER]  disable miner from boot
  |    --list                  list currently running miners
  |    --list-installed        list currently installed miners
  |    --attach [MINER]        open miner session
  |   --help, -h               display help
  | 
  | [MINER] is only mandatory for some sub-arguments
  | 
  | Report any bugs to WheezyBackports#7260 on discord.
  `----


1.3 List Installed Miners
~~~~~~~~~~~~~~~~~~~~~~~~~

  Before starting/stopping or enabling miners on boot we have to make
  sure we have them installed.

  To check if you have a miner installed run mm like so.
  ,----
  | mm --manage-miner --list-installed
  `----

  If you do have a miner installed you will get an output like this.
  ,----
  | ===> teamredminer-v0.10.2
  | ===> /home/addycmos/addy/miners/teamredminer-v0.10.2/init/teamredminer-v0.10.2_service exists
  | ===> /home/addycmos/addy/miners/teamredminer-v0.10.2/teamredminer-v0.10.2_run exists
  `----

  If you don't have a miner installed nothing will come up. I urge you
  to go over the Capman documentation.

  As you can see from earlier I have teamredminer-v0.10.2 installed. We
  can now move onto running this miner.


1.4 Running Miners
~~~~~~~~~~~~~~~~~~

1.4.1 Configuration
-------------------

  To run our miner we're going to want to make sure we have it
  configured first. Miner Manager does not have any features to
  automatically setup your configs for you. It just runs the main start
  script for the miner.

  From our earlier example you can see that I have teamredminer-v0.10.2
  installed. Let's take a look at configuration.  To configure our miner
  we're going to need to cd into miners/teamredminer-v.10.2
  ,----
  | cd miners/teamredminer-v0.10.2
  `----

  Now that we are in the miner directory we can now list the contents to
  see what we're working with.
  ,----
  | ls
  `----

  Contents:
  ,----
  | AUTOLYKOS_TUNING.txt           TRM_DUAL_MINING_API.txt                start_firo_testnet.sh
  | BACK-teamredminer-v0.10.2_run  TRTL_CHUKWA_MINING.txt                 start_kawpow.sh
  | CLOCKS_VOLTAGE_CONTROL.txt     USAGE.txt                              start_mtp.sh
  | CN_AUTOTUNING_WITH_TRM.txt     VERTHASH_TUNING.txt                    start_multipool.sh
  | CN_GENERAL_TUNING.txt          bin                                    start_nimiq.sh
  | CN_MAX_YOUR_VEGA.txt           cl_watchdog.sh                         start_phi2.sh
  | DUAL_ETH_MINING.txt            init                                   start_ton.sh
  | DUAL_ZIL_MINING.txt            run_autotune_full.sh                   start_trtl_chukwa.sh
  | ETHASH_4GB_HOWTO.txt           run_autotune_quick.sh                  start_verthash.sh
  | ETHASH_FROM_0.7_TO_0.8.txt     start_c31.sh                           start_x16rv2.sh
  | ETHASH_TUNING_GUIDE.txt        start_cd29.sh                          start_zil_eth.sh
  | ETHASH_TUNING_GUIDE_RMODE.txt  start_cnr.sh                           start_zil_eth_old_variant.sh
  | FPGA_GUIDE.txt                 start_dual_eth_ton_2miners_tonpool.sh  teamredminer-v0.10.2_run
  | KAWPOW_FIROPOW_TUNING.txt      start_dual_eth_ton_ethermine_ice.sh    ubuntu_mode_script.sh
  | MAP_YOUR_GPUS.txt              start_ergo.sh                          watchdog.sh
  | MTP_MINING.txt                 start_eth.sh                           watchdog_sysrq.sh
  | NIMIQ_MINING.txt               start_eth_4gb.sh
  | TON_MINING.txt                 start_firo.sh
  `----

  Now your contents may not be 1:1 with mine because I have a couple of
  files in here, but we're not worried about those. Because this is Team
  Red Miner configuration will differ from XMRig. To configure XMRig you
  want to edit the config.json. For Team Red though we're going to be
  editing the teamredminer-v0.10.2_run file. We'll be using vim for
  this, but you can install nano or whatever text editor you want. Also
  to keep this simple I won't be using my personal config.
  ,----
  | vim teamredminer-v0.10.2_run
  `----

  Contents:
  ,----
  | #!/bin/sh
  | 
  | # These environment variables should be set to for the driver to allow max mem allocation from the gpu(s).
  | export GPU_MAX_ALLOC_PERCENT=100
  | export GPU_SINGLE_ALLOC_PERCENT=100
  | export GPU_MAX_HEAP_SIZE=100
  | export GPU_USE_SYNC_OBJECTS=1
  | 
  | # Example batch file for starting teamredminer.  Please fill in all <fields> with the correct values for you.
  | # Format for running miner:
  | #      teamredminer.exe -a <algo> -o stratum+tcp://<pool address>:<pool port> -u <pool username/wallet> -p <pool password>
  | #
  | # Fields:
  | #      algo - the name of the algorithm to run. E.g. x16r or x16rv2
  | #      pool address - the host name of the pool stratum or it's IP address. E.g. lux.pickaxe.pro
  | #      pool port - the port of the pool's stratum to connect to.  E.g. 8332
  | #      pool username/wallet - For most pools, this is the wallet address you want to mine to.  Some pools require a username
  | #      pool password - For most pools this can be empty.  For pools using usernames, you may need to provide a password as configured on the pool.
  | #
  | # Example steps:
  | # 1. If you prefer a different pool, change the pool server address.
  | #
  | # 2. Replace the example wallet with your own wallet(!).
  | #
  | # 3. Name your worker by changing "trmtest" to your name of choice after the wallet below.
  | #
  | # 4. You're good to go!
  | 
  | /home/addycmos/addy/miners/teamredminer-v0.10.2/bin/teamredminer-v0.10.2 -a ethash -o stratum+tcp://eu1.ethermine.org:4444 -u 0x02197021fefa795fec661a45f60e47a6f6605281.trmtest -p x
  `----

  I won't tell you what to do here. I'm sure you can figure it out on
  your own just fine since everyone has a different setup. This default
  configuration specifically is for a regular ethereum mining pool. I
  should also mention that I recommend reading the Team Red
  documentation to learn how to use the miner.

  As a bonus if you want to use Team Red on Monero Ocean here's a config
  for it.
  ,----
  | /home/addycmos/addy/miners/teamredminer-v0.10.2/bin/teamredminer-v0.10.2 -a ethash -o stratum+tcp://gulf.moneroocean.stream:10128 -u YOUR_XMR_WALLET_HERE -p YOUR_WORKER_NAME_HERE~ethash --eth_stratum_mode=nicehash
  `----

  With configuration out of the way we can move on to actually running
  the miner.


1.4.2 Actually Running The Miner
--------------------------------

  To start our miner we use Miner Manager like so.
  ,----
  | mm --manage-miner --start teamredminer-v0.10.2
  `----

  You will see an output like so.
  ,----
  | === Starting teamredminer-v0.10.2 ===
  | ===> Service 'teamredminer-v0.10.2_service' started.
  `----


1.5 List Currently Running Miners
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  To get a list of the miners currently running we use Miner Manager
  like so.
  ,----
  | mm --manage-miner --list
  `----

  If a miner is running you should see it come up like so.
  ,----
  | ===> teamredminer-v0.10.2
  `----


1.6 Attaching A Miner Session
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  If a miner is running you can attach the tmux session of that miner
  using Miner Manager. Using Miner Manager to attach the session is the
  recommended way to do it.
  ,----
  | mm --manage-miner --attach teamredminer-v0.10.2
  `----

  Once you attach the session you can now interact with the miner. To
  detach the session use "ctrl-a d".

  After you detach you'll get a message in the shell like this.
  ,----
  | [detached (from session teamredminer-v0_10_2_miner)]
  `----


1.7 Enabling The Miner On System Boot
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  To enable the miner on boot you can use Miner Manager to do so.
  ,----
  | mm --manage-miner --enable-boot teamredminer-v0.10.2
  `----

  You will get a message saying it is enabling the miner on boot.
  ,----
  | === Enabling teamredminer-v0.10.2 on boot ===
  `----

  If the miner is already enabled to run on boot you'll get an error
  message telling you it is already enabled.
  ,----
  | ===> Miner is already enabled on boot!
  `----
