CAPSULE_NAME="xmrig-v6.18.0"
NAME="xmrig"
VERSION="6.18.0"
FULLNAME="${NAME}-${VERSION}-linux-x64"
ARCHIVE="${FULLNAME}.tar.gz"
DESC="XMRig is a high performance CPU/GPU miner."
URL="https://github.com/${NAME}/${NAME}/releases/download/v${VERSION}/${ARCHIVE}"
DEST="/home/addycmos/addy/miners/${CAPSULE_NAME}"

capsule_download() {
	wget -nc ${URL}
}

capsule_extract_archive() {
	tar -xf ${ARCHIVE}
}

capsule_install() {
	if [ ! -d "${DEST}" ]; then
		mkdir ${DEST}
	fi

	if [ ! -d "${DEST}/bin" ]; then
		mkdir ${DEST}/bin
	fi
	
	if [ ! -d "${DEST}/init" ]; then
		mkdir ${DEST}/init
	fi

	sed -i "s/${NAME}/${CAPSULE_NAME}/g" ${CAPSULE_NAME}/${NAME}_run
	
	sed -i "s/${NAME}/${CAPSULE_NAME}/g" ${CAPSULE_NAME}/${NAME}_service

	install -m 755 ${CAPSULE_NAME}/${NAME}-${VERSION}/${NAME} ${DEST}/bin/${CAPSULE_NAME}
	install -m 644 ${CAPSULE_NAME}/${NAME}-${VERSION}/config.json ${DEST}
	install -m 755 ${CAPSULE_NAME}/${NAME}_run ${DEST}/${CAPSULE_NAME}_run
	install -m 644 ${CAPSULE_NAME}/${NAME}_service ${DEST}/init/${CAPSULE_NAME}_service
	install -m 644 ${CAPSULE_NAME}/${NAME}_service /etc/dinit.d/${CAPSULE_NAME}_service

}

