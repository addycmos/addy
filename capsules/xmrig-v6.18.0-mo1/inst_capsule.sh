CAPSULE_NAME="xmrig-v6.18.0-mo1"
NAME="xmrig"
VERSION="6.18.0-mo1"
FULLNAME="${NAME}-v${VERSION}-lin64"
ARCHIVE="${FULLNAME}.tar.gz"
DESC="XMRig MO is a high performance CPU/GPU miner designed for the Monero Ocean pool."
URL="https://github.com/MoneroOcean/${NAME}/releases/download/v${VERSION}/${ARCHIVE}"
DEST="/home/addycmos/addy/miners/${CAPSULE_NAME}"

capsule_download() {
	wget -nc ${URL}
}

capsule_extract_archive() {
	tar -xf ${ARCHIVE}
}

capsule_install() {
	if [ ! -d "${DEST}" ]; then
		mkdir ${DEST}
	fi

	if [ ! -d "${DEST}/bin" ]; then
		mkdir ${DEST}/bin
	fi
	
	if [ ! -d "${DEST}/init" ]; then
		mkdir ${DEST}/init
	fi

	sed -i "s/${NAME}/${CAPSULE_NAME}/g" ${CAPSULE_NAME}/${NAME}_run
	
	sed -i "s/${NAME}/${CAPSULE_NAME}/g" ${CAPSULE_NAME}/${NAME}_service

	install -m 755 ${CAPSULE_NAME}/${NAME} ${DEST}/bin/${CAPSULE_NAME}
	install -m 644 ${CAPSULE_NAME}/config.json ${DEST}
	install -m 755 ${CAPSULE_NAME}/${NAME}_run ${DEST}/${CAPSULE_NAME}_run
	install -m 644 ${CAPSULE_NAME}/${NAME}_service ${DEST}/init/${CAPSULE_NAME}_service
	install -m 644 ${CAPSULE_NAME}/${NAME}_service /etc/dinit.d/${CAPSULE_NAME}_service

}

