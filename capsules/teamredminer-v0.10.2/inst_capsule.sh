CAPSULE_NAME="teamredminer-v0.10.2"
NAME="teamredminer"
VERSION="0.10.2"
FULLNAME="${NAME}-v${VERSION}-linux"
ARCHIVE="${FULLNAME}.tgz"
DESC="Team Red Miner is a high performance AMD GPU miner."
URL="https://github.com/todxx/${NAME}/releases/download/v${VERSION}/${ARCHIVE}"
DEST="/home/addycmos/addy/miners/${CAPSULE_NAME}"

capsule_download() {
	wget -nc ${URL}
}

capsule_extract_archive() {
	tar -xf ${ARCHIVE}
}

capsule_install() {
	if [ ! -d "${DEST}" ]; then
		mkdir ${DEST}
	fi

	if [ ! -d "${DEST}/bin" ]; then
		mkdir ${DEST}/bin
	fi
	
	if [ ! -d "${DEST}/init" ]; then
		mkdir ${DEST}/init
	fi

	sed -i "s/${NAME}/${CAPSULE_NAME}/g" ${CAPSULE_NAME}/${NAME}_run
	
	sed -i "s/${NAME}/${CAPSULE_NAME}/g" ${CAPSULE_NAME}/${NAME}_service

	cp ${CAPSULE_NAME}/${FULLNAME}/*.txt ${DEST}/
	cp ${CAPSULE_NAME}/${FULLNAME}/*.sh ${DEST}/

	install -m 755 ${CAPSULE_NAME}/${FULLNAME}/${NAME} ${DEST}/bin/${CAPSULE_NAME}
	install -m 755 ${CAPSULE_NAME}/${NAME}_run ${DEST}/${CAPSULE_NAME}_run
	install -m 644 ${CAPSULE_NAME}/${NAME}_service ${DEST}/init/${CAPSULE_NAME}_service
	install -m 644 ${CAPSULE_NAME}/${NAME}_service /etc/dinit.d/${CAPSULE_NAME}_service

}

