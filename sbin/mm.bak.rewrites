## Pretty colors
source colors

## Path Variables
MINERS=/home/addycmos/addy/miners

## Runs basic mining setup commands for CPU mining
cpu_configsetup () {
	echo "${YELLOW}=== ${NOCOLOR}Setting up CPU mining config ${YELLOW}===${NOCOLOR}"
	
	## Only ran when a configuration file is specified
	if [ "$USECONFIG" != '' ]; then
		echo "${YELLOW}===> ${NOCOLOR}Using script config ${YELLOW}: ${GREEN}${USECONFIG}${NOCOLOR}";
		source $USECONFIG
	fi
	
	## Sets up chosen miner
	if [ "$MINER" == 'xmrig' ] || [ "$MINER" == 'xmrig-mo' ]; then
		echo "${YELLOW}===> ${NOCOLOR}Using ${GREEN}${MINER} ${NOCOLOR}as CPU miner."
		cp $MINERS/$MINER/def_config.json $MINERS/$MINER/config.json;
		echo "${YELLOW}===> ${NOCOLOR}Copied new ${GREEN}${MINER} ${NOCOLOR}config file from default config.";
		MINERSET="yes"
	else
		echo "${RED}===> ${YELLOW}Please specify ${RED}xmrig ${YELLOW}or ${RED}xmrig-mo. ${NOCOLOR}"
	fi
		
	## User specified wallet address gets added to new miner config
	if [ "$WALLETADDR" != '' ] && [ "$MINERSET" == 'yes' ]; then
		echo "${YELLOW}===> ${NOCOLOR}Using wallet address ${YELLOW}: ${GREEN}${WALLETADDR}${NOCOLOR}";
		sed -i "s/YOUR_WALLET_ADDRESS/$WALLETADDR/g" $MINERS/$MINER/config.json;
		echo "${YELLOW}===> ${NOCOLOR}Wrote wallet address to config.";
		SETUPINIT="yes"
	elif [ "$WALLETADDR" == '' ]; then
		echo "${RED}===> ${YELLOW}You need to specify a wallet address! ${NOCOLOR}"
	fi

	## User specified worker name gets added to new miner config
	if [ "$WORKERNAME" != '' ] && [ "$MINERSET" == 'yes' ]; then
		echo "${YELLOW}===> ${NOCOLOR}Using worker name ${YELLOW}: ${GREEN}${WORKERNAME}${NOCOLOR}";
		sed -i "s/\"x/\"$WORKERNAME/g" $MINERS/$MINER/config.json;
		echo "${YELLOW}===> ${NOCOLOR}Wrote worker name to config."
	elif [ "$WORKERNAME" == '' ]; then
		echo "${RED}===> ${YELLOW}You need to specify a worker name! ${NOCOLOR}"
	fi

	## Change pool to user specified pool
	if [ "$POOLADDR" != '' ] && [ "$MINERSET" == 'yes' ]; then
		sed -i "s/CHOSEN_POOL/$POOLADDR/g" $MINERS/$MINER/config.json;
		echo "${YELLOW}===> ${NOCOLOR}Changed pool address to ${YELLOW}: ${GREEN}${POOLADDR}${NOCOLOR}"
	elif [ "$POOLADDR" == '' ]; then
		echo "${RED}===> ${YELLOW}You need to specify a pool! ${NOCOLOR}"
	fi

	## If desired choose specific algorithm
	if [ "$ALGO" != '' ] && [ "$MINERSET" == 'yes' ] && [ "$MINER" == 'xmrig' ]; then
		echo "${YELLOW}===> ${NOCOLOR}Set algorithm to ${YELLOW}: ${GREEN}${ALGO}${NOCOLOR}"
		sed -i "s/\"algo\"\: null,/\"algo\"\: \"$ALGO\",/g" $MINERS/$MINER/config.json;
	elif [ "$ALGO" != '' ] && [ "$MINERSET" == 'yes' ] && [ "$MINER" == 'xmrig-mo' ]; then
		echo "${YELLOW}===> ${NOCOLOR}Set algorithm to ${YELLOW}: ${GREEN}${ALGO}${NOCOLOR}"
		sed -i "s/$WORKERNAME/$WORKERNAME\~$ALGO/g" $MINERS/$MINER/config.json
	fi

	## Enables logging
	if [ "$LOG" == 'y' ] && [ "$MINERSET" == 'yes' ]; then
		LOGPATH="$MINERS/$MINER/$MINER-log.txt"
		sed -i "s+\"log-file\"\: null,+\"log-file\"\: \"$LOGPATH\",+g" $MINERS/$MINER/config.json
	fi
	
	## Enables miner on boot
	setupinit_func () {
		## Checks for which XMRIG version is running
		XMRIG_CHECK=$(pidof xmrig)
                XMRIGMO_CHECK=$(pidof xmrig-mo)

		## Stops current running miner before proceeding
		if [ "$XMRIG_CHECK" != '' ] || [ "$XMRIGMO_CHECK" != '' ]; then
			stop_miner 
		fi
		
		## Removes XMRIG service files if found for basic cleanup and to prevent wrong XMRIG from running on boot
		if [ -f /etc/dinit.d/boot.d/${MINER}_service ]; then
			disable_boot
		fi
	
		## Enables chosen miner on boot and creates the necessary symlink
		enable_boot

		## Start chosen miner
		start_miner
	}
	
	## If everything goes well then setup init scripts
	if [ "$SETUPINIT" == 'yes' ]; then
		setupinit_func;
	fi
}

## Runs basic mining setup commands for GPU mining
gpu_configsetup () {		
	echo "${YELLOW}=== ${NOCOLOR}Setting up GPU mining config ${YELLOW}===${NOCOLOR}"
	
	## Error function for when user tries to select unknown miner or no miner
	miner_err () {
		echo "${RED}===> ${YELLOW}Please specify GPU miner to use. ${NOCOLOR}"
		exit 1
	}		
	
	## Error function for when user does not provide a proper coin config to use
	config_err () {
		echo "${RED}===> ${YELLOW}Unknown coin config or unspecified coin config.${NOCOLOR}"
		exit 1
	}
	
	## Function for specific teamredminer setup
	trm_setup () {
		echo "${YELLOW}===> ${NOCOLOR}Using ${GREEN}${MINER} ${NOCOLOR} as GPU miner."
		
		## Available valid coin configs for teamredminer
		## When no proper or valid coin config is specified then by default
		## run config_err function
		case "$COINCONFIG" in
			c31)            COINCONFIG=start_c31.sh;;
			cd29)           COINCONFIG=start_cd29.sh;;
			cnr)            COINCONFIG=start_cnr.sh;;
			ergo)           COINCONFIG=start_ergo.sh;;
			eth)            COINCONFIG=start_eth.sh;;
			eth_4gb)        COINCONFIG=start_eth_4gb.sh;;
			firo)           COINCONFIG=start_firo.sh;;
			kawpow)         COINCONFIG=start_kawpow.sh;;
			mtp)            COINCONFIG=start_mtp.sh;;
			nimiq)          COINCONFIG=start_nimiq.sh;;
			phi2)           COINCONFIG=start_phi2.sh;;
			ton)            COINCONFIG=start_ton.sh;;
			trtl_chukwa)    COINCONFIG=start_trtl_chukwa.sh;;
			verthash)       COINCONFIG=start_verthash.sh;;
			x16rv2)         COINCONFIG=start_x16rv2.sh;;
			*)		config_err;;
		esac
		
		## Copy selected default coin config to start_trm.sh script
		cp $MINERS/$MINER/$COINCONFIG $MINERS/$MINER/start_trm.sh
		echo "${YELLOW}===> ${NOCOLOR}Copied new ${GREEN}${MINER} ${NOCOLOR}config file from default config."
			
		## User specified wallet address gets added to new TRM start script
		if [ "$WALLETADDR" != '' ]; then
			echo "${YELLOW}===> ${NOCOLOR}Using wallet address ${YELLOW}: ${GREEN}${WALLETADDR}${NOCOLOR}"
			sed -i "s/-u.*\.trmtest/-u $WALLETADDR/g" $MINERS/$MINER/start_trm.sh
			echo "${YELLOW}===> ${NOCOLOR}Wrote wallet address to config."
		elif [ "$WALLETADDR" == '' ]; then
			echo "${RED}===> ${YELLOW}You need to specify a wallet address! ${NOCOLOR}"
			exit 1
		fi

		## User specified workername gets added to new TRM start script
		if [ "$WORKERNAME" != '' ]; then
			echo "${YELLOW}===> ${NOCOLOR}Using worker name ${YELLOW}: ${GREEN}${WORKERNAME}${NOCOLOR}"
			sed -i "s/-p x/-p $WORKERNAME/g" $MINERS/$MINER/start_trm.sh
			echo "${YELLOW}===> ${NOCOLOR}Wrote worker name to config."
		elif [ "$WORKERNAME" == '' ]; then
			echo "${RED}=== ${YELLOW}You need to specify a worker name! ${NOCOLOR}"
			exit 1
		fi

		## User specified wallet address gets added to new TRM start script
		if [ "$POOLADDR" != '' ]; then
			echo "${YELLOW}===> ${NOCOLOR}Using pool address ${YELLOW}: ${GREEN}${POOLADDR}${NOCOLOR}"
			sed -i "s/-o.*-u/-o $POOLADDR -u/g" $MINERS/$MINER/start_trm.sh
			echo "${YELLOW}===> ${NOCOLOR}Wrote pool address to config."
		elif [ "$POOLADDR" == '' ]; then
			echo "${RED}===> ${YELLOW}You need to specify a pool! ${NOCOLOR}"
			exit 1
		fi
	}
	
	## Available GPU miners
	## If no miner or no valid miner is chosen then run miner_err function
	case $MINER in
		teamredminer)		trm_setup;;
		*)			miner_err;;
	esac
}

## Sets up miner init scripts
setup_init () {
	## Checks if miner is already running
	MINER_PID=$(pidof $MINER)

	## Stops miner if it is running before proceeding
	if [ "$MINER_PID" != '' ]; then
		stop_miner
	fi

	## Removes miner service files if found for basic cleanup
	if [ -f /etc/dinit.d/boot.d/${MINER}_service ]; then
		disable_boot
	fi

	## Enables chosen miner on boot and creates the necessary symlink
	enable_boot

	## Start chosen miner
	
	start_miner
}

## Function for starting chosen miner
start_miner () {
	if [ "$MINER" == 'xmrig' ] || [ "$MINER" == 'xmrig-mo' ]; then
		echo "${YELLOW}=== ${NOCOLOR}Starting ${GREEN}${MINER} ${YELLOW}===${NOCOLOR}"
		START=$(dinitctl start ${MINER}_service)
		echo "${YELLOW}===> ${NOCOLOR}${START}"
	fi
}

## Function for stopping chosen miner
stop_miner () {
	if [ "$MINER" == 'xmrig' ] || [ "$MINER" == 'xmrig-mo' ]; then
		echo "${YELLOW}=== ${NOCOLOR}Stopping ${GREEN}${MINER} ${YELLOW}===${NOCOLOR}"
		STOP=$(dinitctl stop ${MINER}_service)
		echo "${YELLOW}===> ${NOCOLOR}${STOP}"
	fi
}

## Function for enabling chosen miner on system boot
enable_boot () {
	## Detects if xmrig is being invoked then checks if a boot file isn't already present
	if [ "$MINER" == 'xmrig' ] && [ ! -f "/etc/dinit.d/boot.d/xmrig_service" ]; then
		## If chosen miner is xmrig and no boot file is present then create boot file using symbolic link
		echo "${YELLOW}=== ${NOCOLOR}Enabling ${GREEN}${MINER} ${NOCOLOR}on boot ${YELLOW}===${NOCOLOR}"
		ln -s /etc/dinit.d/xmrig_service /etc/dinit.d/boot.d
	## If a boot file is present alert the user xmrig is already enabled on boot
	elif [ "$MINER" == 'xmrig' ] && [ -f "/etc/dinit.d/boot.d/xmrig_service" ]; then
		echo "${RED}===> ${YELLOW}Miner is already enabled on boot! ${NOCOLOR}"
	fi
	
	## Detects if xmrig-mo is being invoked then checks if a boot file isn't already present
	if [ "$MINER" == 'xmrig-mo' ] && [ ! -f "/etc/dinit.d/boot.d/xmrig-mo_service" ]; then
	## If chosen miner is xmrig-mo and no boot file is present then create boot file using symbolic link
		echo "${YELLOW}=== ${NOCOLOR}Enabling ${GREEN}${MINER} ${NOCOLOR}on boot ${YELLOW}===${NOCOLOR}"
		ln -s /etc/dinit.d/xmrig-mo_service /etc/dinit.d/boot.d
	## If a boot file is present alert the user xmrig-mo is already enabled on boot
	elif [ "$MINER" == 'xmrig-mo' ] && [ -f "/etc/dinit.d/boot.d/xmrig-mo_service" ]; then
		echo "${RED}===> ${YELLOW}Miner is already enabled on boot! ${NOCOLOR}"
	fi	
}

## Function for disabling chosen miner from system boot
disable_boot () {
	## Detects if xmrig is being invoked then check if a boot file is present
	if [ "$MINER" == 'xmrig' ] && [ -f "/etc/dinit.d/boot.d/xmrig_service" ]; then
		## if chosen miner is xmrig and a boot file is present then remove boot file
		echo "${YELLOW}=== ${NOCOLOR}Disabling ${GREEN}${MINER} ${NOCOLOR}from boot ${YELLOW}===${NOCOLOR}"
		rm /etc/dinit.d/boot.d/xmrig_service
	## If boot file is not present alert the user xmrig is already disabled from boot
	elif [ "$MINER" == 'xmrig' ] && [ ! -f "/etc/dinit.d/boot.d/xmrig_service" ]; then
		echo "${RED}===> ${YELLOW}Miner is already disabled from boot! ${NOCOLOR}"
	fi

	## Detects if xmrig-mo is being invoked then check if a boot file is present
	if [ "$MINER" == 'xmrig-mo' ] && [ -f "/etc/dinit.d/boot.d/xmrig-mo_service" ]; then
		## if chosen miner is xmrig-mo and a boot file is present then remove boot file
		echo "${YELLOW}=== ${NOCOLOR}Disabling ${GREEN}${MINER} ${NOCOLOR}from boot ${YELLOW}===${NOCOLOR}"
		rm /etc/dinit.d/boot.d/xmrig-mo_service
	## If boot file is not present alert the user xmrig-mo is already disabled from boot
	elif [ "$MINER" == 'xmrig-mo' ] && [ ! -f "/etc/dinit.d/boot.d/xmrig-mo_service" ]; then
		echo "${RED}===> ${YELLOW}Miner is already disabled from boot! ${NOCOLOR}"
	fi	
}


## Command line arguments for miner setup selection
if [ "$1" == '--setup-cpu' ]; then
	## Command line arguments for CPU miner configuration
	for ARGUMENT in "$@"
	do
		KEY=$(echo $ARGUMENT | cut -f1 -d=)
		VALUE=$(echo $ARGUMENT | cut -f2 -d=)

		case "$KEY" in
			WALLETADDR)	WALLETADDR=${VALUE};;
			WORKERNAME)	WORKERNAME=${VALUE};;
			POOLADDR)	POOLADDR=${VALUE};;
			ALGO)		ALGO=${VALUE};;
			LOG)		LOG=${VALUE};;
			USECONFIG)	USECONFIG=${VALUE};;
			MINER)		MINER=${VALUE};;
			*)
		esac
	done
	cpu_configsetup;
fi

if [ "$1" == '--setup-gpu' ]; then
	## Command line arguments for GPU miner configuration
	for ARGUMENT in "$@"
	do
		KEY=$(echo $ARGUMENT | cut -f1 -d=)
		VALUE=$(echo $ARGUMENT | cut -f2 -d=)
		
		case "$KEY" in
			WALLETADDR)	WALLETADDR=${VALUE};;
			WORKERNAME)	WORKERNAME=${VALUE};;
			POOLADDR)	POOLADDR=${VALUE};;
			COINCONFIG)	COINCONFIG=${VALUE};;
			MINER)		MINER=${VALUE};;
			*)
		esac
	done
	gpu_configsetup;
fi

## Command line argument for miner management
if [ "$1" == '--manage-miner' ]; then
	MINER=$3
	if [ "$MINER" == 'xmrig' ] || [ "$MINER" == 'xmrig-mo' ]; then
		case $2 in
			--start)	start_miner;;
			--stop)		stop_miner;;
			--enable-boot)	enable_boot;;
			--disable-boot)	disable_boot;;
		esac		
	else
		echo "Please specify xmrig or xmrig-mo."
	fi
fi

## Display help output
help_output () {
	echo "${GREEN} --help, -h"
        echo "${NOCOLOR} Usage: --help, -h"
        echo "${NOCOLOR} Description: Show script arguments."
	echo "${GREEN} --setup-cpu"
	echo "${NOCOLOR} Usage: --setup-cpu WALLETADDR=Your_Crypto_Wallet_Address WORKERNAME=Your_Workername POOLADDR=pool.address.stream MINER=xmrig"
	echo "${NOCOLOR} Optional Args: ALGO=Algorithm, LOG=y, USECONFIG=Your_Premade_Config"
	echo "${NOCOLOR} Description: Sets up CPU mining on rig. Depending on chosen miner 'ALGO=' will work differently."
	echo "${NOCOLOR} On xmrig-mo it changes the worker name to include ~algorithm. 'LOG=y' turns on logging for the miner."
	echo "${NOCOLOR} 'USCONFIG=' is for using premade config files for setup."
	echo "${GREEN} --manage-miner"
	echo "${NOCOLOR} Usage: --manage-miner --start xmrig, --stop xmrig, --enable-boot xmrig, --disable-boot xmrig"
	echo "${NOCOLOR} Description: For CPU miner management. After configureation with '--setup-cpu' the miner is automatically started" 
	echo "${NOCOLOR} You can attach the miner session within tmux as well."
}

miner_list () {
	echo "${GREEN} List of miners:"
	echo "${NOCOLOR} xmrig"
	echo "${NOCOLOR} xmrig-mo"
}

if [ "$1" == '--help' ] || [ "$1" == '-h' ]; then
	help_output
	miner_list
fi
