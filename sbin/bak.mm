## Pretty colors
source colors

## Path Variables
MINERS=/home/addycmos/addy/miners

## Runs basic mining setup commands
cpu_configsetup () {
	## Only ran when a configuration file is specified
	if [ "$USECONFIG" != '' ]; then
		echo "${YELLOW}=== ${NOCOLOR}Using script config ${YELLOW}: ${GREEN}${USECONFIG} ${YELLOW}===${NOCOLOR}";
		source $USECONFIG
	fi
	
	## Sets up chosen miner
	if [ "$MINER" == 'xmrig' ] || [ "$MINER" == 'xmrig-mo' ]; then
		echo "${YELLOW}=== ${NOCOLOR}Using ${GREEN}${MINER} ${NOCOLOR}as CPU miner. ${YELLOW}===${NOCOLOR}"
		cp $MINERS/$MINER/def_config.json $MINERS/$MINER/config.json;
		echo "${YELLOW}===> ${NOCOLOR}Copied new ${GREEN}${MINER} ${NOCOLOR}config file from default config.";
		MINERSET="yes"
	else
		echo "${RED}===> ${YELLOW}Please specify ${RED}xmrig ${YELLOW}or ${RED}xmrig-mo. ${NOCOLOR}"
	fi
		
	## User specified wallet address gets added to new miner config
	if [ "$WALLETADDR" != '' ] && [ "$MINERSET" == 'yes' ]; then
		echo "${YELLOW}=== ${NOCOLOR}Using wallet address ${YELLOW}: ${GREEN}${WALLETADDR} ${YELLOW}===${NOCOLOR}";
		sed -i "s/YOUR_WALLET_ADDRESS/$WALLETADDR/g" $MINERS/$MINER/config.json;
		echo "${YELLOW}===> ${NOCOLOR}Wrote wallet address to config.";
		SETUPINIT="yes"
	elif [ "$WALLETADDR" == '' ]; then
		echo "${RED}===> ${YELLOW}You need to specify a wallet address! ${NOCOLOR}"
	fi

	## User specified worker name gets added to new miner config
	if [ "$WORKERNAME" != '' ] && [ "$MINERSET" == 'yes' ]; then
		echo "${YELLOW}=== ${NOCOLOR}Using worker name ${YELLOW}: ${GREEN}${WORKERNAME} ${YELLOW}===${NOCOLOR}";
		sed -i "s/\"x/\"$WORKERNAME/g" $MINERS/$MINER/config.json;
		echo "${YELLOW}===> ${NOCOLOR}Wrote worker name to config."
	elif [ "$WORKERNAME" == '' ]; then
		echo "${RED}===> ${YELLOW}You need to specify a worker name! ${NOCOLOR}"
	fi

	## Change pool to user specified pool
	if [ "$POOLADDR" != '' ] && [ "$MINERSET" == 'yes' ]; then
		sed -i "s/CHOSEN_POOL/$POOLADDR/g" $MINERS/$MINER/config.json;
		echo "${YELLOW}=== ${NOCOLOR}Changed pool address to ${YELLOW}: ${GREEN}${POOLADDR} ${YELLOW}===${NOCOLOR}"
	elif [ "$POOLADDR" == '' ]; then
		echo "${RED}===> ${YELLOW}You need to specify a pool! ${NOCOLOR}"
	fi

	## If desired choose specific algorithm
	if [ "$ALGO" != '' ] && [ "$MINERSET" == 'yes' ] && [ "$MINER" == 'xmrig' ]; then
		echo "${YELLOW}=== ${NOCOLOR}Set algorithm to ${YELLOW}: ${GREEN}${ALGO} ${YELLOW}===${NOCOLOR}"
		sed -i "s/\"algo\"\: null,/\"algo\"\: \"$ALGO\",/g" $MINERS/$MINER/config.json;
	elif [ "$ALGO" != '' ] && [ "$MINERSET" == 'yes' ] && [ "$MINER" == 'xmrig-mo' ]; then
		echo "${YELLOW}=== ${NOCOLOR}Set algorithm to ${YELLOW}: ${GREEN}${ALGO} ${YELLOW}===${NOCOLOR}"
		sed -i "s/$WORKERNAME/$WORKERNAME\~$ALGO/g" $MINERS/$MINER/config.json
	fi

	## Enables logging
	if [ "$LOG" == 'y' ] && [ "$MINERSET" == 'yes' ]; then
		LOGPATH="$MINERS/$MINER/$MINER-log.txt"
		sed -i "s+\"log-file\"\: null,+\"log-file\"\: \"$LOGPATH\",+g" $MINERS/$MINER/config.json
	fi
	
	## Enables miner on boot
	setupinit_func () {
		echo "${YELLOW}=== ${NOCOLOR}Enabling ${GREEN}${MINER} ${NOCOLOR}on boot ${YELLOW}===${NOCOLOR}"
		## Checks for which XMRIG version is running
		XMRIG_CHECK=$(pidof xmrig)
		XMRIGMO_CHECK=$(pidof xmrig-mo)
		
		## These two if statements keep anything funky from happening when the user switched miners
		## If stock XMRIG is detected then stop xmrig_service
		if [ "$XMRIG_CHECK" != '' ]; then
			dinitctl stop xmrig_service
		fi
		
		## If Monero Ocean XMRIG is detected then stop xmrig-mo_service
		if [ "$XMRIGMO_CHECK" != '' ]; then
			dinitctl stop xmrig-mo_service
		fi
		
		## Removes XMRIG service files for basic cleanup and to prevent wrong XMRIG from running
		## If stock XMRIG service is found them remove it
		if [ -f /etc/dinit.d/boot.d/xmrig_service ]; then
			rm /etc/dinit.d/boot.d/xmrig_service
		fi

		## If Monero Ocean XMRIG service is found then remove it
		if [ -f /etc/dinit.d/boot.d/xmrig-mo_service ]; then
			rm /etc/dinit.d/boot.d/xmrig-mo_service
		fi

		## These two statements are uses for creating the service for the chosen miner.
		## If stock XMRIG is used then set the service script to run on boot then start service
		if [ "$MINER" == 'xmrig' ]; then
			ln -s /etc/dinit.d/xmrig_service /etc/dinit.d/boot.d
		#	cp $MINERS/$MINER/init/xmrig_service /etc/dinit.d/xmrig_service
			dinitctl start xmrig_service
		fi
		## If Monero Ocean XMRIG is used then set the service script to run on boot then start service
		if [ "$MINER" == 'xmrig-mo' ]; then
			ln -s /etc/dinit.d/xmrig-mo_service /etc/dinit.d/boot.d
		#	cp $MINERS/$MINER/init/xmrig-mo_service /etc/dinit.d/xmrig-mo_service
			dinitctl start xmrig-mo_service
		fi

	}
	
	## If everything goes well then setup init scripts
	if [ "$SETUPINIT" == 'yes' ]; then
		setupinit_func;
	fi
}

start_miner () {
	if [ "$MINER" == 'xmrig' ] || [ "$MINER" == 'xmrig-mo' ]; then
		echo "${YELLOW}=== ${NOCOLOR}Starting ${GREEN}${MINER} ${YELLOW}===${NOCOLOR}"
		START=$(dinitctl start ${MINER}_service)
		echo "${YELLOW}===> ${NOCOLOR}${START}"
	fi
}

stop_miner () {
	if [ "$MINER" == 'xmrig' ] || [ "$MINER" == 'xmrig-mo' ]; then
		echo "${YELLOW}=== ${NOCOLOR}Stopping ${GREEN}${MINER} ${YELLOW}===${NOCOLOR}"
		STOP=$(dinitctl stop ${MINER}_service)
		echo "${YELLOW}===> ${NOCOLOR}${STOP}"
	fi
}

enable_boot () {
	if [ "$MINER" == 'xmrig' ] && [ ! -f "/etc/dinit.d/boot.d/xmrig_service" ]; then
		echo "${YELLOW}=== ${NOCOLOR}Enabling ${GREEN}${MINER} ${NOCOLOR}on boot ${YELLOW}===${NOCOLOR}"
		cp /etc/dinit.d/xmrig_service /etc/dinit.d/boot.d
	elif [ "$MINER" == 'xmrig' ] && [ -f "/etc/dinit.d/boot.d/xmrig_service" ]; then
		echo "${RED}===> ${YELLOW}Miner is already enabled on boot! ${NOCOLOR}"
	fi

	if [ "$MINER" == 'xmrig-mo' ] && [ ! -f "/etc/dinit.d/boot.d/xmrig-mo_service" ]; then
		echo "${YELLOW}=== ${NOCOLOR}Enabling ${GREEN}${MINER} ${NOCOLOR}on boot ${YELLOW}===${NOCOLOR}"
		cp /etc/dinit.d/xmrig-mo_service /etc/dinit.d/boot.d
	elif [ "$MINER" == 'xmrig-mo' ] && [ -f "/etc/dinit.d/boot.d/xmrig-mo_service" ]; then
		echo "${RED}===> ${YELLOW}Miner is already enabled on boot! ${NOCOLOR}"
	fi	
}

disable_boot () {
	if [ "$MINER" == 'xmrig' ] && [ -f "/etc/dinit.d/boot.d/xmrig_service" ]; then
		echo "${YELLOW}=== ${NOCOLOR}Disabling ${GREEN}${MINER} ${NOCOLOR}from boot ${YELLOW}===${NOCOLOR}"
		rm /etc/dinit.d/boot.d/xmrig_service
	elif [ "$MINER" == 'xmrig' ] && [ ! -f "/etc/dinit.d/boot.d/xmrig_service" ]; then
		echo "${RED}===> ${YELLOW}Miner is already disabled from boot! ${NOCOLOR}"
	fi

	if [ "$MINER" == 'xmrig-mo' ] && [ -f "/etc/dinit.d/boot.d/xmrig-mo_service" ]; then
		echo "${YELLOW}=== ${NOCOLOR}Disabling ${GREEN}${MINER} ${NOCOLOR}from boot ${YELLOW}===${NOCOLOR}"
		rm /etc/dinit.d/boot.d/xmrig-mo_service
	elif [ "$MINER" == 'xmrig-mo' ] && [ ! -f "/etc/dinit.d/boot.d/xmrig-mo_service" ]; then
		echo "${RED}===> ${YELLOW}Miner is already disabled from boot! ${NOCOLOR}"
	fi	
}


## Command line arguments for setup selection
if [ "$1" == '--setup-cpu' ]; then
	## Command line arguments for CPU miner configuration
	for ARGUMENT in "$@"
	do
		KEY=$(echo $ARGUMENT | cut -f1 -d=)
		VALUE=$(echo $ARGUMENT | cut -f2 -d=)

		case "$KEY" in
			WALLETADDR)	WALLETADDR=${VALUE};;
			WORKERNAME)	WORKERNAME=${VALUE};;
			POOLADDR)	POOLADDR=${VALUE};;
			ALGO)		ALGO=${VALUE};;
			LOG)		LOG=${VALUE};;
			USECONFIG)	USECONFIG=${VALUE};;
			MINER)		MINER=${VALUE};;
			*)
		esac
	done
	cpu_configsetup;
fi
## Command line argument for miner management
if [ "$1" == '--manage-miner' ]; then
		MINER=$3
		if [ "$MINER" == 'xmrig' ] || [ "$MINER" == 'xmrig-mo' ]; then
			case $2 in
				--start)	start_miner;;
				--stop)		stop_miner;;
				--enable-boot)	enable_boot;;
				--disable-boot)	disable_boot;;
			esac		
		else
			echo "Please specify xmrig or xmrig-mo."
		fi
fi

help_output () {
	echo "${GREEN} --help, -h"
        echo "${NOCOLOR} Usage: --help, -h"
        echo "${NOCOLOR} Description: Show script arguments."
	echo "${GREEN} --setup-cpu"
	echo "${NOCOLOR} Usage: --setup-cpu WALLETADDR=Your_Crypto_Wallet_Address WORKERNAME=Your_Workername POOLADDR=pool.address.stream MINER=xmrig"
	echo "${NOCOLOR} Optional Args: ALGO=Algorithm, LOG=y, USECONFIG=Your_Premade_Config"
	echo "${NOCOLOR} Description: Sets up CPU mining on rig. Depending on chosen miner 'ALGO=' will work differently."
	echo "${NOCOLOR} 	      On xmrig-mo it changes the worker name to include ~algorithm. 'LOG=y' turns on logging for the miner."
	echo "${NOCOLOR} 	      'USCONFIG=' is for using premade config files for setup."
	echo "${GREEN} --manage-miner"
	echo "${NOCOLOR} Usage: --manage-miner --start xmrig, --stop xmrig, --enable-boot xmrig, --disable-boot xmrig"
	echo "${NOCOLOR} Description: For CPU miner management. After configureation with '--setup-cpu' the miner is automatically started" 
	echo "${NOCOLOR}              You can attach the miner session within tmux as well."
}

if [ "$1" == '--help' ]; then
	help_output
fi
