## Pretty colors
source colors
source message

## Path Variables
MINERS=/home/addycmos/addy/miners

## Runs basic mining setup commands for CPU mining
cpu_configsetup () {
	title "Setting up CPU mining config"
	
	## Only ran when a configuration file is specified
	if [ "$USECONFIG" != '' ]; then
		echo "${YELLOW}===> ${NOCOLOR}Using script config ${YELLOW}: ${GREEN}${USECONFIG}${NOCOLOR}";
		source $USECONFIG
	fi
	
	## Sets up chosen miner
	if [ "$MINER" == 'xmrig' ] || [ "$MINER" == 'xmrig-mo' ]; then
		message "Using ${GREEN}${MINER} ${NOCOLOR}as CPU miner"
		cp $MINERS/$MINER/def_config.json $MINERS/$MINER/config.json;
		message "Copied new ${GREEN}${MINER} ${NOCOLOR}config file from default config.";
		MINERSET="yes"
	else
		error_message "Please specify ${RED}xmrig ${YELLOW}or ${RED}xmrig-mo."
	fi
		
	## User specified wallet address gets added to new miner config
	if [ "$WALLETADDR" != '' ] && [ "$MINERSET" == 'yes' ]; then
		message "Using wallet address ${YELLOW}: ${GREEN}${WALLETADDR}";
		sed -i "s/YOUR_WALLET_ADDRESS/$WALLETADDR/g" $MINERS/$MINER/config.json;
		message "Wrote wallet address to config.";
		SETUPINIT="yes"
	elif [ "$WALLETADDR" == '' ]; then
		error_message "You need to specify a wallet address!"
	fi

	## User specified worker name gets added to new miner config
	if [ "$WORKERNAME" != '' ] && [ "$MINERSET" == 'yes' ]; then
		message "Using worker name ${YELLOW}: ${GREEN}${WORKERNAME}";
		sed -i "s/\"x/\"$WORKERNAME/g" $MINERS/$MINER/config.json;
		message "Wrote worker name to config."
	elif [ "$WORKERNAME" == '' ]; then
		error_message "You need to specify a worker name!"
	fi

	## Change pool to user specified pool
	if [ "$POOLADDR" != '' ] && [ "$MINERSET" == 'yes' ]; then
		sed -i "s/CHOSEN_POOL/$POOLADDR/g" $MINERS/$MINER/config.json;
		message "Changed pool address to ${YELLOW}: ${GREEN}${POOLADDR}"
	elif [ "$POOLADDR" == '' ]; then
		error_message "You need to specify a pool!"
	fi

	## If desired choose specific algorithm
	if [ "$ALGO" != '' ] && [ "$MINERSET" == 'yes' ] && [ "$MINER" == 'xmrig' ]; then
		message "Set algorithm to ${YELLOW}: ${GREEN}${ALGO}"
		sed -i "s/\"algo\"\: null,/\"algo\"\: \"$ALGO\",/g" $MINERS/$MINER/config.json;
	elif [ "$ALGO" != '' ] && [ "$MINERSET" == 'yes' ] && [ "$MINER" == 'xmrig-mo' ]; then
		message "Set algorithm to ${YELLOW}: ${GREEN}${ALGO}"
		sed -i "s/$WORKERNAME/$WORKERNAME\~$ALGO/g" $MINERS/$MINER/config.json
	fi

	## Enables logging
	if [ "$LOG" == 'y' ] && [ "$MINERSET" == 'yes' ]; then
		LOGPATH="$MINERS/$MINER/$MINER-log.txt"
		sed -i "s+\"log-file\"\: null,+\"log-file\"\: \"$LOGPATH\",+g" $MINERS/$MINER/config.json
	fi
	
	## If everything goes well then setup init scripts
	if [ "$SETUPINIT" == 'yes' ]; then
		setup_init;
	fi
}

## Runs basic mining setup commands for GPU mining
gpu_configsetup () {		
	echo "${YELLOW}=== ${NOCOLOR}Setting up GPU mining config ${YELLOW}===${NOCOLOR}"
	
	## Error function for when user tries to select unknown miner or no miner
	miner_err () {
		echo "${RED}===> ${YELLOW}Please specify GPU miner to use. ${NOCOLOR}"
		exit 1
	}		
	
	## Error function for when user does not provide a proper coin config to use
	config_err () {
		echo "${RED}===> ${YELLOW}Unknown coin config or unspecified coin config.${NOCOLOR}"
		exit 1
	}
	
	## Function for specific teamredminer setup
	trm_setup () {
		echo "${YELLOW}===> ${NOCOLOR}Using ${GREEN}${MINER} ${NOCOLOR} as GPU miner."
		
		## Available valid coin configs for teamredminer
		## When no proper or valid coin config is specified then by default
		## run config_err function
		case "$COINCONFIG" in
			c31)            COINCONFIG=start_c31.sh;;
			cd29)           COINCONFIG=start_cd29.sh;;
			cnr)            COINCONFIG=start_cnr.sh;;
			ergo)           COINCONFIG=start_ergo.sh;;
			eth)            COINCONFIG=start_eth.sh;;
			eth_4gb)        COINCONFIG=start_eth_4gb.sh;;
			firo)           COINCONFIG=start_firo.sh;;
			kawpow)         COINCONFIG=start_kawpow.sh;;
			mtp)            COINCONFIG=start_mtp.sh;;
			nimiq)          COINCONFIG=start_nimiq.sh;;
			phi2)           COINCONFIG=start_phi2.sh;;
			ton)            COINCONFIG=start_ton.sh;;
			trtl_chukwa)    COINCONFIG=start_trtl_chukwa.sh;;
			verthash)       COINCONFIG=start_verthash.sh;;
			x16rv2)         COINCONFIG=start_x16rv2.sh;;
			*)		config_err;;
		esac
		
		## Copy selected default coin config to start_trm.sh script
		cp $MINERS/$MINER/$COINCONFIG $MINERS/$MINER/start_trm.sh
		message "Copied new ${GREEN}${MINER} ${NOCOLOR}config file from default config."
		sed -i "s|\./teamredminer|\./$MINERS/$MINER/teamredminer|g" $MINERS/$MINER/start_trm.sh

		## User specified wallet address gets added to new TRM start script
		if [ "$WALLETADDR" != '' ]; then
			message "Using wallet address ${YELLOW}: ${GREEN}${WALLETADDR}"
			sed -i "s/-u.*\.trmtest/-u $WALLETADDR/g" $MINERS/$MINER/start_trm.sh
			message "Wrote wallet address to config."
		elif [ "$WALLETADDR" == '' ]; then
			error_message "You need to specify a wallet address!"
			exit 1
		fi

		## User specified workername gets added to new TRM start script
		if [ "$WORKERNAME" != '' ]; then
			message "Using worker name ${YELLOW}: ${GREEN}${WORKERNAME}"
			sed -i "s/-p x/-p $WORKERNAME/g" $MINERS/$MINER/start_trm.sh
			echo "${YELLOW}===> ${NOCOLOR}Wrote worker name to config."
		elif [ "$WORKERNAME" == '' ]; then
			error_message "You need to specify a worker name!"
			exit 1
		fi

		## User specified wallet address gets added to new TRM start script
		if [ "$POOLADDR" != '' ]; then
			message "Using pool address ${YELLOW}: ${GREEN}${POOLADDR}"
			sed -i "s/-o.*-u/-o stratum\+tcp:\/\/$POOLADDR -u/g" $MINERS/$MINER/start_trm.sh
			message "Wrote pool address to config."
		elif [ "$POOLADDR" == '' ]; then
			error_message "You need to specify a pool!"
			exit 1
		fi

		setup_init
	}
	
	## Available GPU miners
	## If no miner or no valid miner is chosen then run miner_err function
	case $MINER in
		teamredminer)		trm_setup;;
		*)			miner_err;;
	esac
}

## Sets up miner init scripts
setup_init () {
	## Checks if miner is already running
	MINER_PID=$(pidof $MINER)

	## Stops miner if it is running before proceeding
	if [ "$MINER_PID" != '' ]; then
		stop_miner
	fi

	## Removes miner service files if found for basic cleanup
	if [ -f /etc/dinit.d/boot.d/${MINER}_service ]; then
		disable_boot
	fi

	## Enables chosen miner on boot and creates the necessary symlink
	enable_boot

	## Start chosen miner
	
	start_miner
}

## Function for starting chosen miner
start_miner () {
	if [ ! -f "/etc/dinit.d/${MINER}_service" ]; then
		echo "Service file doesn't exist for $MINER"
		exit 1
	fi
	title "Starting ${GREEN}${MINER}"
	START=$(dinitctl start ${MINER}_service)
	message "${NOCOLOR}${START}"
}

## Function for stopping chosen miner
stop_miner () {
	if [ ! -f "/etc/dinit.d/${MINER}_service" ]; then
		echo "Service file doesn't exist for $MINER"
		exit 1
	fi
	title "Stopping ${GREEN}${MINER}"
	STOP=$(dinitctl stop ${MINER}_service)
	message "${NOCOLOR}${STOP}"
}

## Function for enabling chosen miner on system boot
enable_boot () {
	## Detects if boot file is or isn't already present
	if [ ! -f "/etc/dinit.d/boot.d/${MINER}_service" ]; then
		## If no boot file is present then create one using symbolic link
		title "Enabling ${GREEN}${MINER} ${NOCOLOR}on boot"
		ln -s /etc/dinit.d/${MINER}_service /etc/dinit.d/boot.d
	## If a boot file is present alert the user the miner is already enabled on boot
	elif [ -f "/etc/dinit.d/boot.d/${MINER}_service" ]; then
		error_message "Miner is already enabled on boot!"
	fi
}

## Function for disabling chosen miner from system boot
disable_boot () {
	## Detects if boot file is or isn't already present
	if [ -f "/etc/dinit.d/boot.d/${MINER}_service" ]; then
		## If boot file is present then remove boot file
		title "Disabling ${GREEN}${MINER} ${NOCOLOR}from boot"
		rm /etc/dinit.d/boot.d/${MINER}_service
	## If boot file is not present alert the user miner is already disabled from boot
	elif [ ! -f "/etc/dinit.d/boot.d/${MINER}_service" ]; then
		error_message "Miner is already disabled from boot!"
	fi
}


## Command line arguments for miner setup selection
if [ "$1" == '--setup-cpu' ]; then
	## Command line arguments for CPU miner configuration
	for ARGUMENT in "$@"
	do
		KEY=$(echo $ARGUMENT | cut -f1 -d=)
		VALUE=$(echo $ARGUMENT | cut -f2 -d=)

		case "$KEY" in
			WALLETADDR)	WALLETADDR=${VALUE};;
			WORKERNAME)	WORKERNAME=${VALUE};;
			POOLADDR)	POOLADDR=${VALUE};;
			ALGO)		ALGO=${VALUE};;
			LOG)		LOG=${VALUE};;
			USECONFIG)	USECONFIG=${VALUE};;
			MINER)		MINER=${VALUE};;
			*)
		esac
	done
	cpu_configsetup;
fi

if [ "$1" == '--setup-gpu' ]; then
	## Command line arguments for GPU miner configuration
	for ARGUMENT in "$@"
	do
		KEY=$(echo $ARGUMENT | cut -f1 -d=)
		VALUE=$(echo $ARGUMENT | cut -f2 -d=)
		
		case "$KEY" in
			WALLETADDR)	WALLETADDR=${VALUE};;
			WORKERNAME)	WORKERNAME=${VALUE};;
			POOLADDR)	POOLADDR=${VALUE};;
			COINCONFIG)	COINCONFIG=${VALUE};;
			MINER)		MINER=${VALUE};;
			*)
		esac
	done
	gpu_configsetup;
fi

## Command line argument for miner management
if [ "$1" == '--manage-miner' ]; then
	MINER=$3
	
	## Checks if chosen miner is valid
	## If chosen miner is not valid then tell the user to 
	## specify a valid one then exit with a status of 1
	case $MINER in
		xmrig)		;;
		xmrig-mo)	;;
		teamredminer)	;;
		*)		error_message "Please specify a valid miner."; exit 1;;
	esac
	
	case $2 in
		--start)	start_miner;;
		--stop)		stop_miner;;
		--enable-boot)	enable_boot;;
		--disable-boot)	disable_boot;;
	esac		
fi

## Display help output
help_output () {
	echo "${GREEN} --help, -h"
        echo "${NOCOLOR} Usage: --help, -h"
        echo "${NOCOLOR} Description: Show script arguments."
	echo "${GREEN} --setup-cpu"
	echo "${NOCOLOR} Usage: --setup-cpu WALLETADDR=Your_Crypto_Wallet_Address WORKERNAME=Your_Workername POOLADDR=pool.address.stream MINER=xmrig"
	echo "${NOCOLOR} Optional Args: ALGO=Algorithm, LOG=y, USECONFIG=Your_Premade_Config"
	echo "${NOCOLOR} Description: Sets up CPU mining on rig. Depending on chosen miner 'ALGO=' will work differently."
	echo "${NOCOLOR} On xmrig-mo it changes the worker name to include ~algorithm. 'LOG=y' turns on logging for the miner."
	echo "${NOCOLOR} 'USCONFIG=' is for using premade config files for setup."
	echo "${GREEN} --manage-miner"
	echo "${NOCOLOR} Usage: --manage-miner --start xmrig, --stop xmrig, --enable-boot xmrig, --disable-boot xmrig"
	echo "${NOCOLOR} Description: For CPU miner management. After configureation with '--setup-cpu' the miner is automatically started" 
	echo "${NOCOLOR} You can attach the miner session within tmux as well."
}

miner_list () {
	miner_list=("xmrig" "xmrig-mo" "teamredminer")
	echo "${GREEN} List of miners:"
	for i in {0..2}; do
		echo " ${miner_list[i]}"
	done
}

if [ "$1" == '--help' ] || [ "$1" == '-h' ]; then
	help_output
	miner_list
fi
