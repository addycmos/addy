# Directory containing installed mining software
MINERS=/home/addycmos/addy/miners

MINER=$2
MINER_TMUX_COMPAT=$(echo ${MINER} | sed 's/\./_/g') # Have to do a funny sed because tmux converts "." to "_"

# Function for starting xmrig
start_miner() {
	# Create new tmux session for miner and run under that session
	tmux new -d -s ${MINER}_miner $MINERS/$MINER/${MINER}_run
	sleep 1
	check_for_int &
}

# Function for stopping miner
stop_miner() {
	# Check if miner session exists
	MINER_EXIST=$(tmux list-panes -a -F '#S' | grep ${MINER_TMUX_COMPAT}_miner 2> /dev/null)

	# If session of miner still exists kill it
	if [ "$MINER_EXIST" != '' ]; then
		tmux kill-session -t ${MINER_TMUX_COMPAT}_miner
	fi
}

# Function for checking if an interrupt has been sent to the miner or the miner has stopped
check_for_int() {
	# Get current PID of miner then store it for the until loop
	MINER_PID=$(pidof $MINER)
	
	# If $MINER_PID turns up null don't run the until loop 
	if [ "$MINER_PID" != '' ]; then
		# Until $MINER_PID is null meaning the miner got killed or interrupted
		# keep checking if it's still active
		until [ "$MINER_PID" == '' ]; do MINER_PID=$(pidof $MINER); done
		
		## This code is ran only after the until loop ends ##
			
		# Stops the miner service, so the system doesn't think the miner is still running
		dinitctl stop ${MINER}_service
	fi
}

# Pick a card any card! No, but seriously these are just arguments to run the start/stop functions
case $1 in
	start)			start_miner $MINER;;
	stop)			stop_miner $MINER;;
esac
